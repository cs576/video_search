#!/usr/bin/env python

'''
Lucas-Kanade tracker
====================
Lucas-Kanade sparse optical flow demo. Uses goodFeaturesToTrack
for track initialization and back-tracking for match verification
between frames.
Usage
-----
lk_track.py [<video_source>]
Keys
----
ESC - exit
'''

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2 as cv
import video
from common import anorm2, draw_str
from time import clock
import difflib
import os
import numpy as np
import pandas as pd
from scipy.io import wavfile
import os
import fnmatch
from collections import OrderedDict
import sys
NUM_FRAMES = 600
DATADIR = os.path.join(os.path.dirname(__file__), "data")


lk_params = dict(winSize=(15, 15),
                 maxLevel=2,
                 criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

feature_params = dict(maxCorners=500,
                      qualityLevel=0.3,
                      minDistance=7,
                      blockSize=7)


def get_wave_file_paths(rootDir=os.path.join(DATADIR, "databse_videos")):
    fname_dict = {}
    for dirName, subdirList, fileList in os.walk(rootDir):
        # print('Found directory: %s' % dirName)
        for fname in fileList:
            file_name = os.path.join(dirName, fname)
            if '.wav' in fname:
                fname_dict[file_name] = find('*.rgb', dirName)

    return fname_dict

def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def open_wave_file(filepath=os.path.join(DATADIR, "query")):
    """  loads wav file """
    fs, data = wavfile.read(filepath)
    df = pd.DataFrame(data, columns=["left", "right"])
    df["mono"] = 0.5 * (df.left + df.right)
    return df

def norm(x):
    """
    >>> b=[3,4]
    >>> norm(b)
    >>> norm(np.array(b))
    5.0
    """
    return np.sqrt((x**2).sum())

def correlation(x, y):
    x = np.array(x)
    y = np.array(y)
    normx = norm(x)
    normy = norm(y)
    return x.dot(y) / normx / normy

def correlation_with_q(v):
    return correlation(correlation_with_q.q, v)

correlation_with_q.q = []

def query_audio(q, df):
    correlation_with_q.q = q
    idx = df.mono_std.rolling(len(q)).apply(correlation_with_q).idxmax()
    return df.frame_number.loc[idx]
    # return df.iloc[idx - len(q) + 1:idx + 1]

def index_audio(rootDir=os.path.join(DATADIR, "databse_videos")):
    files_to_process = {}
    files_to_process = get_wave_file_paths(rootDir)
    audio_frames = []
    for audio_file_name, image_file_names in files_to_process.items():
        df = open_wave_file(audio_file_name)
        sample_size = len(df.index)
        print(sample_size)
        print(len(image_file_names))
        samples_per_frame = int(sample_size / len(image_file_names))
        print(samples_per_frame)
        for i, image_file_name in enumerate(image_file_names):
            aa = df.left[i * samples_per_frame:(i + 1) * samples_per_frame - 1].std()
            bb = df.right[i * samples_per_frame:(i + 1) * samples_per_frame - 1].std()
            cc = df.mono[i * samples_per_frame:(i + 1) * samples_per_frame - 1].std()
            audio_frames.append([audio_file_name, image_file_name, i, aa, bb, cc])
    return pd.DataFrame(audio_frames, columns="audio_file image_file_name  frame_number left_std right_std mono_std".split())

def save_data(rootDir=os.path.join(DATADIR, "databse_videos")):
    df = index_audio(rootDir)
    df.to_csv(os.path.join(rootDir, "..", "audio.csv"))

def process_query(queryDir):
    """ return the """
    query_result = []
    query_df = index_audio(queryDir)
    query = query_df.mono_std
    df = pd.read_csv("/Users/debankur/Desktop/multimediaproject/audio.csv", index_col=0)
    df_groups = df.groupby("audio_file")
    for file_name, audio_file_df in df_groups:
        frame_number_at_end_of_window = query_audio(query, audio_file_df)
        correlation_with_q.q = query
        # print(audio_file.left_std.iloc[frame_number_at_end_of_window -
        #                               len(query):frame_number_at_end_of_window])
        # print(audio_file.left_std.iloc[1:600])
        correlation = correlation_with_q(
            audio_file_df.mono_std.iloc[
                frame_number_at_end_of_window - len(query) + 1:frame_number_at_end_of_window + 1])
        query_result.append([correlation, file_name, frame_number_at_end_of_window])
    return query_result





class App:
    def __init__(self, video_src):
        self.track_len = 10
        self.detect_interval = 5
        self.tracks = []
        self.cam = video.create_capture(video_src)
        self.frame_idx = 0

    def run(self):
        list1 = []
        while True:

            temp =0
            final_count = 0
            _ret, frame = self.cam.read()
            if(_ret is False) or frame is False:
                thefile = open('sports.txt', 'w')
                for item in list1:
                    thefile.write("%s\n" % item)
                return list1


            frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

            vis = frame.copy()

            if len(self.tracks) > 0:

                img0, img1 = self.prev_gray, frame_gray
                p0 = np.float32([tr[-1] for tr in self.tracks]).reshape(-1, 1, 2)
                p1, _st, _err = cv.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
                p0r, _st, _err = cv.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
                d = abs(p0 - p0r).reshape(-1, 2).max(-1)
                good = d < 1
                new_tracks = []
                for tr, (x, y), good_flag in zip(self.tracks, p1.reshape(-1, 2), good):
                    if not good_flag:
                        continue
                    tr.append((x, y))
                    if len(tr) > self.track_len:
                        del tr[0]
                    new_tracks.append(tr)
                    cv.circle(vis, (x, y), 2, (0, 255, 0), -1)
                self.tracks = new_tracks
                cv.polylines(vis, [np.int32(tr) for tr in self.tracks], False, (0, 255, 0))
                draw_str(vis, (20, 20), 'track count: %d' % len(self.tracks))
                #text_file = open("db.txt", "w")

                final_count = final_count + len(self.tracks)
                #print(final_count)
                list1.append(final_count)
                #print(list1)
                #text_file.write(str(final_count)+'\n')




            if self.frame_idx % self.detect_interval == 0:
                mask = np.zeros_like(frame_gray)
                mask[:] = 255
                for x, y in [np.int32(tr[-1]) for tr in self.tracks]:
                    cv.circle(mask, (x, y), 5, 0, -1)
                p = cv.goodFeaturesToTrack(frame_gray, mask=mask, **feature_params)
                if p is not None:
                    for x, y in np.float32(p).reshape(-1, 2):
                        self.tracks.append([(x, y)])

            self.frame_idx += 1
            self.prev_gray = frame_gray
            cv.namedWindow('lk_track', cv.WINDOW_NORMAL)
            cv.imshow('lk_track', vis)

            ch = cv.waitKey(1)

            if ch == 27:
                break

def find_window():
    DBList =[]
    dict ={}

    starcraft = open('starcraft.txt').readlines()
    dict["starcraft"] = starcraft

    query = open('second_query.txt').readlines()


    flowers = open('flowers.txt').readlines()
    dict["flowers"] = flowers

    musicvideo = open('musicvideo.txt').readlines()
    dict["musicvideo"] = musicvideo

    movie = open('movie.txt').readlines()
    dict["movie"] = movie

    interview = open('interview.txt').readlines()
    dict["interview"] = interview

    sports = open('sports.txt').readlines()
    dict["sports"] = interview

    traffic = open('traffic.txt').readlines()
    dict["traffic"] = traffic

    DBList.append(starcraft)
    DBList.append(flowers)
    DBList.append(musicvideo)
    DBList.append(movie)
    DBList.append(interview)
    DBList.append(sports)
    DBList.append(traffic)
    final_list=[]

    output = difflib.get_close_matches(query, DBList,len(DBList),0)
    counter =0
    for x in output:
        for key, value in dict.items():
            if value == x:
                pass
               #print(value)
               #print(query)
                #print(difflib.SequenceMatcher(None,query,value).ratio())
                final_list.append(key)

    thefile = open('final_rank.txt', 'w')
    for item in final_list:
        thefile.write("%s\n" % item)
    thefile.close()

    deb_array = []
    with open("final_rank.txt", "r") as ins:
        for line in ins:
            deb_array.append(line.rstrip('\n'))
    print(deb_array)

    arzu_array = []
    with open("arzu_rank.txt", "r") as ins:
        for line in ins:
            arzu_array.append(line.rstrip('\n'))
    print(arzu_array)

    vinit_rank = []
    with open("vinit_rank.txt", "r") as ins:
        for line in ins:
            vinit_rank.append(line.rstrip('\n'))
    print(vinit_rank)

    released = {
        "starcraft": 0,
        "sports": 0,
        "movie": 0,
        "traffic": 0,
        "interview": 0,
        "musicvideo": 0,
        "flowers": 0,
    }
    for i in range(len(deb_array)):
        if deb_array[i] == 'starcraft':
            released["starcraft"] = released.get('starcraft') +i
        elif deb_array[i] == 'sports':
            released["sports"] = released.get('sports') +i
        elif deb_array[i] == 'movie':
            released["movie"] = released.get('movie') +i
        elif deb_array[i] == 'traffic':
            released["traffic"] = released.get('traffic') +i
        elif deb_array[i] == 'interview':
            released["interview"] = released.get('interview') +i
        elif deb_array[i] == 'musicvideo':
            released["musicvideo"] = released.get('musicvideo') +i
        elif deb_array[i] == 'flowers':
            released["flowers"] = released.get('flowers') +i

    for i in range(len(arzu_array)):
        if arzu_array[i] == 'starcraft':
            released["starcraft"] = released.get('starcraft') +i
        elif arzu_array[i] == 'sports':
            released["sports"] = released.get('sports') +i
        elif arzu_array[i] == 'movie':
            released["movie"] = released.get('movie') +i
        elif arzu_array[i] == 'traffic':
            released["traffic"] = released.get('traffic') +i
        elif arzu_array[i] == 'interview':
            released["interview"] = released.get('interview') +i
        elif arzu_array[i] == 'musicvideo':
            released["musicvideo"] = released.get('musicvideo') +i
        elif arzu_array[i] == 'flowers':
            released["flowers"] = released.get('flowers') +i

    for i in range(len(vinit_rank)):
        if vinit_rank[i] == 'starcraft':
            released["starcraft"] = released.get('starcraft') +i
        elif vinit_rank[i] == 'sports':
            released["sports"] = released.get('sports') +i
        elif vinit_rank[i] == 'movie':
            released["movie"] = released.get('movie') +i
        elif vinit_rank[i] == 'traffic':
            released["traffic"] = released.get('traffic') +i
        elif vinit_rank[i] == 'interview':
            released["interview"] = released.get('interview') +i
        elif vinit_rank[i] == 'musicvideo':
            released["musicvideo"] = released.get('musicvideo') +i
        elif vinit_rank[i] == 'flowers':
            released["flowers"] = released.get('flowers') +i

    #print(sorted(released.items(), key=lambda x: x[1]))
    d_sorted_by_value = OrderedDict(sorted(released.items(), key=lambda x: x[1]))

    for k, v in d_sorted_by_value.items():
        print("%s: %s" % (k, v))


    #print(released)




    #print(final_list)






def main(queryDir="/Users/debankur/Desktop/multimediaproject/databse_videos"):
    #index_audio("/Users/debankur/Desktop/multimediaproject/database_videos/")
    #save_data("/Users/debankur/Desktop/multimediaproject/databse_videos/")
    #queryDir = "/Users/debankur/Desktop/multimediaproject/query/first/"
    #query_result = process_query("/Users/debankur/Desktop/multimediaproject/query/first")
    #audio_frames = query_result
    #query_result = sorted(audio_frames, reverse=True)[:3]
    # print(query_result)



    video_src = 'sports.mp4'

    #video_sport = 'sports.mp4'
    #video_movie = 'movie.mp4'

    #video_query = 'first_query.mp4'


   # print(__doc__)
    #db_musicvideo =App(video_src).run()
    #query_list = App(video_test).run()
    #db_sport = App(video_src).run()
    #query_test = App(video_test).run()

    find_window()



    #print(db_sport)
    #print(query_list)
    #print(db_musicvideo)
    #App(video_src).run()
    #print(db_list)
    #print(query_list)
    #App(video_test).run()
    #print(temp)
    #print(list1)
    #cv.destroyAllWindows()


if __name__ == '__main__':
    main()